# Conferences presentations

## Rencontres R 2023

* Poster [SK8 : Gestion et hébergement d'application R Shiny](https://hal.science/hal-04141247v1)

## useR! 2022

[Session](https://user2022.r-project.org/program/posters/#posters-on-r-in-the-wild)  
20-23 June 2022 (All virtual)  

{VMR} Virtual Machines for/with R  

* Poster [{VMR} Virtual Machines For/With R, J.F. Rey](JF_REY_UseR2022_VMR.pdf)  
* [HAL](https://hal.archives-ouvertes.fr/hal-03701831v1)  

Keywords : R package, virtualbox, vagrant, packer, ansible, gitlab ci/cd  

## Rencontres R 2021  

[RencontresR2021](https://rr2021.sciencesconf.org/program)  
12-13 July 2021 - AgroParisTech, Paris  

* Slides [Environnement R et GitLab CI/CD, J.F. Rey, L. Houde](GitLabCICD_R_RENCONTRESR2021.pdf)  
* [HAL](https://hal.archives-ouvertes.fr/hal-03286897)  
* [Video presentation](https://youtu.be/D5mE8AaRnw0)  

## JDEV 2020

[jdev2020](http://devlog.cnrs.fr/jdev2020)  
6 - 10 July 2020, Rennes, (All virtual)

### REX

* [GitLab CI/CD | Environnement R, J.F. Rey](GitLabCICD_R_JDEV2020.pdf) | [HAL](https://hal.archives-ouvertes.fr/hal-03286897v1) 
* [Slides with audio](slide_JF_REY_GITLABCICD_R.mp4)  
* [Video presentation](https://www.canal-u.tv/video/jdev/jdev2020_t4_gitlab_ci_cd_pour_le_langage_r.57591)  

### Poster

* [GitLab CI/CD et environnement R, J.F. Rey, L. Houde](JF_REY-JDEV2020-GitLab_R.pdf)
* [https://hal.archives-ouvertes.fr/hal-02899373](https://hal.archives-ouvertes.fr/hal-02899373)

### Working Group

* [R, outils et usines logicielles](http://devlog.cnrs.fr/jdev2020/t4.gt02)  
  * [Support](T4GT02_slide.pdf)  
  * [CR](T4GT02_Note.md)  

## useR! 2019  

[useR!2019](https://www.user2019.fr/)  
9 - 12 July 2019, Toulouse FR  

* Poster [R Package Development using GitLab CI/CD, J.F. Rey, L. Houde](useR2019-R_packages_GitLab_JF_REY.pdf)   
* [HAL](https://hal.archives-ouvertes.fr/hal-02284507)  


## OLDER Refs

[https://gitlab.paca.inrae.fr/r-ecosystem/docs](https://gitlab.paca.inrae.fr/r-ecosystem/docs)  

